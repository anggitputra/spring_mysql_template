package com.microservice.starter.repository;

import com.microservice.starter.entity.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductRepositoryTest {

    @Autowired
    ProductRepository productRepository;

    @Test
    void name() {
        List<Product> productList = (List<Product>) productRepository.findAll();
        System.out.println(productList.size());
    }
}