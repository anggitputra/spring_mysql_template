package com.microservice.starter.service;

import com.microservice.starter.entity.Product;
import com.microservice.starter.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductServiceTest {

    @Autowired ProductService productService;

    @Test
    void productList() {
        List<Product> productList = productService.productList();
        System.out.println(productList.size());
    }


    @Autowired
    ProductRepository productRepository;

    @Test
    void name() {
        List<Product> productList = (List<Product>) productRepository.findAll();
        System.out.println(productList.size());
    }
}