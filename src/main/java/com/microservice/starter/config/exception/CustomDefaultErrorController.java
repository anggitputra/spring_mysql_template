package com.microservice.starter.config.exception;

import com.microservice.starter.config.GlobalApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/error")
@Slf4j
public class CustomDefaultErrorController extends AbstractErrorController {

    public CustomDefaultErrorController(final ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping
    public ResponseEntity<?> error(final HttpServletRequest request, CustomException e) {
        log.warn("CDEC-E-31 : "+e.getMessage());
        return
                GlobalApiResponse.builder()
                        .message(this.getStatus(request).getReasonPhrase())
                        .status(this.getStatus(request).value())
                        .error(this.getStatus(request).getReasonPhrase())
                        .build().entity();
    }
}
