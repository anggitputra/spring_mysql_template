package com.microservice.starter.config.exception;

import com.microservice.starter.config.GlobalApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;


@RestControllerAdvice
@Slf4j
public class ErrorHandler {

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<?> handleNoHandlerFound(NoHandlerFoundException ce) {
        log.warn("EH-HNHF-19 : "+ce.getMessage());
        return
                GlobalApiResponse.builder()
                        .message(HttpStatus.NOT_FOUND.getReasonPhrase())
                        .status(HttpStatus.NOT_FOUND.value())
                        .error(HttpStatus.NOT_FOUND.getReasonPhrase())
                        .build().entity();
    }

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<?> handleCustomException(CustomException ce) {
        log.warn("EH-HCE-30 : "+ce.getMessage());
        return
                GlobalApiResponse.builder()
                        .message(ce.getMessage())
                        .status(ce.getHttpStatus().value())
                        .error(ce.getHttpStatus().getReasonPhrase())
                        .build().entity();
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception e) {
        log.warn("EH-HE-52 : "+ e.getMessage());
        return
                GlobalApiResponse.builder()
                        .message(HttpStatus.BAD_REQUEST.getReasonPhrase())
                        .status(HttpStatus.BAD_REQUEST.value())
                        .error(HttpStatus.BAD_REQUEST.getReasonPhrase())
                        .build().entity();
    }


}
