package com.microservice.starter.controller;

import com.google.gson.Gson;
import com.microservice.starter.config.GlobalApiResponse;
import com.microservice.starter.config.exception.CustomException;
import com.microservice.starter.dto.DtoProductReq;
import com.microservice.starter.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@Validated
public class ProductController {
    Gson gson = new Gson();

    @PostMapping(value = "products", produces = MediaType.APPLICATION_JSON_VALUE)
    public GlobalApiResponse<?> saleTransaction(
            @RequestBody DtoProductReq request
    ) {
        log.debug("Transaction Request" +  gson.toJson(request));
        Product product = new Product();
        try {
            product.setId(1);
            product.setName(request.getName());
            product.setPrice(request.getPrice());
            product.setColour(request.getColour());
            product.setStock(request.getStock());
        }catch (Exception e){
            throw new CustomException("Data Not Allowed", HttpStatus.BAD_REQUEST);
        }
        log.debug("Transaction Response" + gson.toJson(product));
        return new GlobalApiResponse<>(
                HttpStatus.OK.value(),
                product
        );
    }

}
