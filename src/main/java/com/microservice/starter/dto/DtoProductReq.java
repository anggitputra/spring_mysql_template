package com.microservice.starter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtoProductReq {
    String name;
    Integer price;
    String colour;
    Integer stock;
}
