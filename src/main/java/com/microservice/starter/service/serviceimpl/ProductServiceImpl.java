package com.microservice.starter.service.serviceimpl;

import com.microservice.starter.entity.Product;
import com.microservice.starter.repository.ProductRepository;
import com.microservice.starter.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> productList() {
        List<Product> productList = (List<Product>) productRepository.findAll();
        return productList;
    }
}
