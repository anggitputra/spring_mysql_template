package com.microservice.starter.service;

import com.microservice.starter.entity.Product;
import org.springframework.stereotype.Service;

import java.util.List;

public interface ProductService {

    List<Product> productList();
}
